﻿using System;

int[] numbers = new int[6];
int count = 0;

bool exit = false; 

while (!exit)  
{
  Console.OutputEncoding = System.Text.Encoding.UTF8;
  Console.WriteLine("1. Xem danh sách");
  Console.WriteLine("2. Thêm số mới");
  Console.WriteLine("3. Thoát");
   
  switch (Convert.ToInt32(Console.ReadLine()))
  {
    case 1:
      Console.OutputEncoding = System.Text.Encoding.UTF8;
      Console.WriteLine("Danh sách các số: ");
      for(int i = 0; i < count; i++)  
      {
        Console.Write(numbers[i] + " ");    
      }
      Console.WriteLine();
      break;
      
    case 2:
      int newNumber;
      do    
      {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.Write("Nhập số cần thêm (6 chữ số): "); 
        newNumber = Convert.ToInt32(Console.ReadLine());
        
      } while(newNumber.ToString().Length != 6);
      
      if(count == numbers.Length) 
        Array.Resize(ref numbers, numbers.Length * 2);  

      numbers[count++] = newNumber;
      break;
    
    case 3:
      exit = true;
      break;
  }
}